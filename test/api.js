import {resolve} from 'path'
import assert from 'assert'
import {transform} from 'babel-core'
import plugin from '../src'

describe('API', () => {
  it('should support custom parsing of the path', () => {
    const fixture = 'import { random } from "pokemon";'
    const {code: actual} = transform(fixture, {plugins: [
      [plugin, {
        parse: () => 'superheroes'
      }]
    ]})
    const expected = 'import { random } from "superheroes";'
    assert.equal(actual, expected)
  })

  it('should pass the correct arguments to `parse()`', (done) => {
    const fixture = 'import { random } from "pokemon";'
    const {code: actual} = transform(fixture, {filename: 'snorlax', plugins: [
      [plugin, {
        parse: (basedir, dependency, source, filename) => {
          assert.equal(resolve(basedir), resolve(process.cwd()), 'basedir')
          assert.equal(resolve(dependency), resolve('node_modules/pokemon/index.js'), 'dependency')
          assert.equal(source, 'pokemon', 'source')
          assert.equal(filename, 'snorlax', 'filename')
          done()
        }
      }]
    ]})
  })
})
