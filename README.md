# babel-plugin-transform-import-resolve

Resolve import paths for named dependencies

## Example

**In**

```js
import derp from 'derp'
```

**Out**

```js
import derp from './node_modules/derp/lib/index.js'
```

## Installation

```sh
$ npm install babel-plugin-transform-import-resolve
```

## Usage

### Via `.babelrc` (Recommended)

**.babelrc**

```json
{
  "plugins": ["transform-import-resolve"]
}
```

### Via CLI

```sh
$ babel --plugins transform-import-resolve script.js
```

### Via Node API

```javascript
require('babel-core').transform('code', {
  plugins: ['transform-import-resolve']
})
```

To override how the path is transformed, pass a Function to the `parse` option:

```js
const options = {
  function parse (basedir, dependency, source, filename) {
    return path.relative(basedir, dependency)
  }
}

require('babel-core').transform('code', {
  plugins: ['transform-import-resolve', options]
})
```
