import slash from 'slash'
import {dirname, relative} from 'path'
import {sync as resolve} from 'resolve'

function parseResolve (...args) {
  const resolved = relative(...args)
  const prefixed = (resolved.startsWith('.') || resolved.startsWith('/')) ?
    resolved : './' + resolved
  const forward = slash(prefixed)
  return forward
}

function resolver (
  {node: {source}},
  {
    file: {opts: {filename}},
    opts: {parse = parseResolve}
  }
) {
  if (source !== null) {
    const basedir = dirname(filename)
    let dependency
    try {
      dependency = resolve(source.value, {basedir})
    } catch (err) {
      return
    }
    source.value = parse(basedir, dependency, source.value, filename)
  }
}

function transformImportResolve () {
  return {
    visitor: {
      ExportNamedDeclaration: resolver,
      ImportDeclaration: resolver
    }
  }
}

module.exports = transformImportResolve
